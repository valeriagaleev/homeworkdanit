/*
Доповніть код так, щоб він коректно працював
*/

const array = ["value", () => "showValue"];

// Допишіть код тут

const [value, showValue] = array;

alert(value); // має бути виведено 'value'
alert(showValue()); // має бути виведено 'showValue'
