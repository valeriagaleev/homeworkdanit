"use strict";

window.addEventListener('keydown', (event) => {
    document.querySelectorAll('.btn').forEach(elems => {
        elems.style.backgroundColor = 'black';
    })
    let buttons = Array.from(document.querySelectorAll('.btn')).find(elems => {
        return elems.innerHTML.toLowerCase() === event.key.toLowerCase();
    })
    buttons ? buttons.style.backgroundColor = 'blue' : false;
})