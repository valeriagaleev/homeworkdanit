/*Опишіть, як можна створити новий HTML тег на сторінці.
З допомогою методу document.createElement.
Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
Цей параметр виизначає місце де буде розташування новоствореного елемента('beforebegin': перед ел.,'afterbegin':всередині ел. перед першим потомком, 'beforeend':всередині ел. перед останнім потомком,'afterend':після ел.).
Як можна видалити елемент зі сторінки?
За допомогою методу .remove().
*/

"use strict";

let arr = ["1", "2", "3", "sea", "user", 23];
const divInBody = document.createElement('div');

function newList(x, y = document.body) {
    y.insertAdjacentHTML("afterbegin", `<ul>${x.map(elem => {
        return `<li>${elem}</li>`
    }).join('')}</ul>`);
}
document.body.append(divInBody);
newList(["1", "2", "3", "sea", "user", 23], divInBody);
console.log(arr);


