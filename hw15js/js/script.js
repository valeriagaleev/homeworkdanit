/*
Напишіть, як ви розумієте рекурсію. Навіщо вона використовується на практиці?
Рекурсия - это вызов функции самой себя, а также вызов функций, которые лежат в теле другой функции. С помощью рекурсии существенно упрощается код и решение задач. 
*/

let num1 = +prompt('Enter the first number');
let num2 = +prompt('Enter the second number');

while (!num1 || !num2 || Number.isNaN(+num1, +num2)) {
    alert('Enter your number!'); 
    num1 = prompt('Необходимо ввести первое число!', num1);
    num2 = prompt('Необходимо ввести второе число!', num2);
}

function recursion (a,b) {
    return (b == 1) ? a : (a * recursion(a, b - 1));
}
console.log(recursion(num1, num2));