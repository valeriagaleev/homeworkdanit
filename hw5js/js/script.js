/*
Опишіть своїми словами, що таке метод об'єкту.
Іншими словами, метод - це функція, яка є членом об'єкта. З їх допомогою можна виконувати дії з самим об'єктом.
Який тип даних може мати значення властивості об'єкта?
Властивість є значення або набір значень (у вигляді масиву або об'єкта), який є членом об'єкта і може містити будь який тип даних.
Об'єкт це посилальний тип даних. Що означає це поняття?
Тобто сам об'єкт дає посилання на якесь значення. 
*/

"use strict";

function createNewUser() {
    let firstName = prompt('Enter your name.');
    let lastName = prompt('Enter your last name.');

    const newUser = {
        firstName,
        lastName,
        getLogin() {
            return (this.firstName.charAt(0) + this.lastName).toLowerCase();
        },
        setFirstName(name) {
            Object.defineProperty(this, 'firstName', {value: name });
        },
        setLasttName(name) {
            Object.defineProperty(this, 'lastName', {value: name });
        },
    };

    Object.defineProperties(newUser, {
        firstName: { writable: false },
        lastName: { writable: false },
    })

    return newUser;
}

const userObj = createNewUser();

console.log(userObj);
console.log(userObj.getLogin());


