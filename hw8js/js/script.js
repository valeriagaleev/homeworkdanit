/*Опишіть своїми словами що таке Document Object Model (DOM)
Це структура сторінки HTML описана js об'єктом.
Яка різниця між властивостями HTML-елементів innerHTML та innerText?
InnerText показує весь текст який міститься в елементі, InnerHTML покаже окрім тексту, також розмітку HTML.
Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
За допомогою методів пошуку: getElementsByClassName,getElementsByTagName,querySelector,querySelectorAll,getElementById,getElementsByName...Найбільш популярні querySelector,querySelectorAll.
*/

"use strict";

let pCollection = document.getElementsByTagName("p");
console.log(pCollection);
for (let i = 0; i < pCollection.length; i++) {
  pCollection[i].style.backgroundColor = "#ff0000";
}
let elById = document.querySelector("#optionsList");
let parentNode = elById.parentElement;
console.log(parentNode);
let nodes = elById.childNodes;
for (let node of nodes) {
  console.log(node.nodeName, node.nodeType);
}
let content = document.querySelectorAll("#testParagraph");
content.forEach((el) => (el.textContent = "This is a paragraph"));
let parentEl = elById.closest("div");
console.log(parentEl);

let elMainHeader = document.querySelector(".main-header");
console.log(elMainHeader);
let children = elMainHeader.children;
console.log(children);

let elSecTitle = document.getElementsByClassName("section-title");
while (elSecTitle[0]) {
  elSecTitle[0].parentNode.removeChild(elSecTitle[0]);
  console.log(elSecTitle);
}
console.log("byTagName", pCollection);
console.log("byId", elById);
console.log("parent", parentEl);
console.log(content);
