const url = "https://ajax.test-danit.com/api/swapi/films";

const root = document.querySelector("#root");
const wrapperDiv = document.createElement("div");
wrapperDiv.classList.add("wrapper");
root.append(wrapperDiv);

axios
  .get(url)
  .then(({ data }) => {
    data.forEach((elem) => {
      const { name, episodeId, openingCrawl } = elem;

      new ListStarWars(name, episodeId, openingCrawl).render(wrapperDiv);
    });
    const nameFilmStarWars = document.querySelectorAll(".list__name");
    const newNameFilmStarWars = [...nameFilmStarWars];

    newNameFilmStarWars.forEach((elemFilm, index) => {
      const listCharacters = document.createElement("ol");
      const loader = document.createElement("div");
      elemFilm.after(loader, listCharacters);
      loaderSpiner(loader);

      Promise.allSettled(data[index].characters).then((res) => {
        res.forEach(({ status, value }) => {
          if (status === "fulfilled") {
            axios
              .get(value)
              .then(({ data }) => {
                loader.remove();
                listCharacters.insertAdjacentHTML(
                  "beforeend",
                  `<li>${data.name}</li>`
                );
              })
              .catch((err) => {
                console.log(err);
              });
          }
        });
      });
    });
  })
  .catch((err) => {
    console.warn(err);
  });

class ListStarWars {
  constructor(name, episodeId, openingCrawl) {
    this.name = name;
    this.episodeId = episodeId;
    this.openingCrawl = openingCrawl;
    this.listStarWarsElem = document.createElement("ul");
  }

  createHtml(container) {
    this.listStarWarsElem.classList.add("list");
    container.append(this.listStarWarsElem);
  }

  render(container) {
    this.createHtml(container);
    this.listStarWarsElem.insertAdjacentHTML(
      "beforeend",
      `
        <li class="list__item">
            <p class="list__episode">
                <span class="list__status">Namber episode:</span> ${this.episodeId}</p>
            <p class="list__name">
                <span class="list__status">Film:</span> ${this.name}</p>
            <p class="list__desc">
                <span class="list__status">Description:</span> ${this.openingCrawl}</p>
        </li>
    `
    );
  }
}

function loaderSpiner(loader) {
  loader.insertAdjacentHTML(
    "beforeend",
    `
      <div class="container">
        <span class="circle"></span>
        <span class="circle"></span>
        <span class="circle"></span>
        <span class="circle"></span>
      </div>
    `
  );
}
