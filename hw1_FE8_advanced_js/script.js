
class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }

  set name(value) {
    return (this._name = value);
  }

  get age() {
    return this._age;
  }

  set age(value) {
    return (this._age = value);
  }

  get salary() {
    return this._salary;
  }

  set salary(value) {
    return (this._salary = value);
  }
}

const employee = new Employee("Valeria", 29, 2000);
console.log(employee);

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }

  get salary() {
    return super.salary * 3;
  }
}

const programmer = new Programmer("Pavel", 26, 1300, "de");
console.log(programmer);
console.log(`My name ${programmer.name}. My age ${programmer.age}. My salary ${programmer.salary}. I know ${programmer.lang} language.`);

const programmer2 = new Programmer("Polina", 24, 1500, "en");
console.log(programmer2);
console.log(`My name ${programmer2.name}. My age ${programmer2.age}. My salary ${programmer2.salary}. I know ${programmer2.lang} language.`);

const programmer3 = new Programmer("Denis", 31, 1700, "ua");
console.log(programmer3);
console.log(`My name ${programmer3.name}. My age ${programmer3.age}. My salary ${programmer3.salary}. I know ${programmer3.lang} language.`);