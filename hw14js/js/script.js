"use strict";

let body = document.querySelector('body');
let btnChangeColor = document.createElement('button');
btnChangeColor.innerHTML = 'change color';
body.prepend(btnChangeColor);

btnChangeColor.addEventListener('click', (event) => {
    if(event) {
        localStorage.setItem('color theme', body.style.backgroundColor === 'white' ? body.style.backgroundColor = 'pink' : body.style.backgroundColor = 'white');
    }
})