"use strict";

let tabs = document.querySelector('.tabs');
let listItems = document.querySelectorAll('.tabs-content li');


tabs.addEventListener('click', (event) => {
    console.log(tabs.querySelector('.active'));
    tabs.querySelector('.active').classList.remove('active');
    event.target.classList.add('active');
    const elemId = event.target.id;
    listItems.forEach(item => {
        item.hidden = true;
        if (elemId === item.dataset.filter) {
            item.hidden = false;
        }
    })
})
