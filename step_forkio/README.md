Список використаних технологій:

    "browser-sync": "^2.27.10",
    "del": "^6.0.0",
    "gulp": "^4.0.2",
    "gulp-autoprefixer": "^8.0.0",
    "gulp-clean-css": "^4.3.0",
    "gulp-concat": "^2.6.1",
    "gulp-file-include": "^2.3.0",
    "gulp-group-css-media-queries": "^1.2.2",
    "gulp-imagemin": "^8.0.0",
    "gulp-js-minify": "^0.0.3",
    "gulp-newer": "^1.4.0",
    "gulp-rename": "^2.0.0",
    "gulp-replace": "^1.1.3",
    "gulp-sass": "^5.1.0",
    "sass": "^1.56.1"

Склад учасників проекту:

Віталій Єрмоленко:

Зверстати шапку сайту з верхнім меню (включаючи випадаюче меню при малій роздільній здатності екрана.
Зверстати секцію People Are Talking About Fork.


Валерія Євенко:

Зверстати блок Revolutionary Editor. Кнопки треба зробити, щоб виглядали як тут справа вгорі (звідти ж за допомогою інспектора можна взяти всі SVG іконки і завантажити стилі, що використовуються на гітхабі).
Зверстати секцію Here is what you get.
Зверстати секцію Fork Subscription Pricing. У блоці з цінами третій елемент завжди буде "виділений" і буде більшим за інші (тобто не по кліку/ховеру, а статично).


Опис проєкту:
Збірка проєкту відбувається за допомогою звичайного gulp.

Команди:
npm run dev - запуск проєкту в режимі development
npm run build - запуск проєкту в режимі production
