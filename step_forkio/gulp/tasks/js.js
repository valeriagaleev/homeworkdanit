import rename from 'gulp-rename';
import concat from 'gulp-concat';
import minifyjs from 'gulp-js-minify';



export const js = () =>{
    return app.gulp.src(app.path.src.js, {sourcemaps: true })
    .pipe(concat("main.js"))
    .pipe(minifyjs())
    .pipe(rename({
        extname: ".min.js"
    }))
    .pipe(app.gulp.dest(app.path.build.js))
    .pipe(app.plugins.browsersync.stream());
}