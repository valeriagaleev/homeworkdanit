const findBtn = document.querySelector('.find-by-ip');

const params = ['timezone', 'country', 'region', 'city', 'zip']

findBtn.addEventListener('click', async () => {
    const responseIP = await fetch('https://api.ipify.org/?format=json')
    const ip = (await responseIP.json()).ip;

    const responseIPInfo = await fetch(`http://ip-api.com/json/${ip}`);
    const ipInfo = await responseIPInfo.json();

    const ipInfoList = document.createElement('ul');

    for (const param of params) {
        const ipInfoItem = document.createElement('li');
        ipInfoItem.textContent = `${param}: ${ipInfo[param]}`;
        ipInfoList.append(ipInfoItem);
    }

    document.body.append(ipInfoList);
})


