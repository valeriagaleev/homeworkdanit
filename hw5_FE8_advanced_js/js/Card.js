class Card {
    constructor({id, userId, title, body}) {
        this._id = id;
        this._userId = userId;
        this._title = title;
        this._body = body;
    }

    render() {
        this._cardNode = createElement({type: 'div', classList: 'card', id: this._id});
        this._cardNode.innerHTML =
            `<h3 class="card-title">${this._title}</h3>
            <p class="card-body">${this._body}</p>
            <p class="user-info"></p>
            <button class="btn change-card-btn">Change card</button>
            <button class="btn delete-card-btn">Delete card</button>`

        this._userInfo = this._cardNode.querySelector('.user-info');

        const changeCardBtn = this._cardNode.querySelector('.change-card-btn');
        changeCardBtn.addEventListener('click', event => {
            this.changeCard(event.target.parentElement)
        });

        const deleteCardBtn = this._cardNode.querySelector('.delete-card-btn');
        deleteCardBtn.addEventListener('click', (event) => {
            this.deleteCard(event.target.parentElement)
        })

        return this._cardNode;
    }

    renderUserInfo(promiseUser, parentNode) {
        return promiseUser.then(user => {
            this._user = user;
            this._userNameNode = createElement({type: 'p', classList: 'card-user-name', text: this._user.name});

            this._userEmailNode = createElement({type: 'a', classList: 'card-user-email', text: this._user.email});
            this._userEmailNode.setAttribute('href', `mailto:${user.email}`);

            parentNode.append(this._userNameNode, this._userEmailNode)
        })
    }

    changeCard(post) {
        const formContainer = showForm('Change post', 'Update')
        const form = formContainer.firstElementChild;
        const postId = post.id;
        form.addEventListener('submit', (event) => {
            event.preventDefault();
            const newTitle = form.querySelector('#title').value;
            const newBody = form.querySelector('#body').value;
            fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
                    method: 'PATCH',
                    body: JSON.stringify({
                        "title": newTitle,
                        "body": newBody
                    })
                }
            )
                .then(response => {
                    if (response.ok) {
                        post.querySelector('.card-title').textContent = newTitle;
                        post.querySelector('.card-body').textContent = newBody;
                    }
                })
                .then(() => {
                    formContainer.remove()
                })
        })
    }

    deleteCard(card) {
        fetch(`https://ajax.test-danit.com/api/json/posts/${card.id}`, {method: 'DELETE'})
            .then(response => {
                if (response.ok) {
                    card.remove();
                }
            })
    }

    get userId() {
        return this._userId;
    }

    get userInfo() {
        return this._userInfo;
    }

}

Card.getAllCard = (url) => {
    return fetch(url)
        .then(response => response.json())
        .then(cards => cards.map(card => new Card(card)))
}

Card.renderCardList = (promisePost, parentNode) => {
    const spinner = createElement({type:'div', classList:'lds-dual-ring'});
    parentNode.append(spinner);

    promisePost
        .then(cards => {
            const cardsNode = createElement({type:'div', classList:'cards'});
            const promiseList = [];
            cards.forEach(card => {
                const cardNode = card.render();
                promiseList.push(card.renderUserInfo(User.getUserById(card.userId), card.userInfo));
                cardsNode.append(cardNode);
            })
            Promise.all(promiseList).then(() => {
                parentNode.append(cardsNode)
                spinner.remove();
            })
        })
}

Card.addNewCard = () => {
    const formContainer = showForm('Add new post', 'Create')
    const form = formContainer.firstElementChild

    form.addEventListener('submit', (event) => {
        event.preventDefault();
        fetch('https://ajax.test-danit.com/api/json/posts', {
            method: 'POST',
            body: JSON.stringify({
                "userId": 1,
                "title": form.querySelector('#title').value,
                "body": form.querySelector('#body').value
            })
        })
            .then(response => response.json())
            .then(card => {
                const newCard = new Card(card);
                document.querySelector('.cards').prepend(newCard.render());
                newCard.renderUserInfo(User.getUserById(newCard.userId), newCard.userInfo);
            })
            .then(() => {
                formContainer.remove()
            })
    })
}