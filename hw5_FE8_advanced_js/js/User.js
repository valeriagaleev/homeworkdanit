class User {
    constructor({id, name, username, email, address, phone, website, company}) {
        this._id = id;
        this._name = name;
        this._username = username;
        this._email = email;
        this._address = address;
        this._phone = phone;
        this._website = website;
        this._company = company;
    }

    get name() {
        return this._name;
    }

    get email() {
        return this._email;
    }

}

User.getUserById = (id) => {
    return fetch(`https://ajax.test-danit.com/api/json/users/${id}`)
        .then(response => response.json())
        .then(user => new User(user))
}