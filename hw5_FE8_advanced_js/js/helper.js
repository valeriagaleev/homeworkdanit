const createElement = ({type, text, classList, id}) => {
    const elem = document.createElement(type);
    elem.className = classList ? classList : '';
    elem.textContent = text ? text : '';
        elem.setAttribute('id', id ? id : '');
    return elem;
}