/*
Як можна оголосити змінну у Javascript?
Потрібно оголосити назву змінної: let, const або var(цю краще не використовувати, бо вона застаріла); 
У чому різниця між функцією prompt та функцією confirm?
promt - показує повідомлення з проханням дати відповідь на це запитання, також йому можна задати дефолтне значення, завдяки цій функції ми отримаємо результат який ввів користувач або ж якщо він натиснув відміну ми отримаємо значення null. confirm - показує користувачу наше запитання і дає змогу відповісти: ок (повертає для нас значення true) або скасувати (повертає false);
Що таке неявне перетворення типів? Наведіть один приклад.
Це примусове перетворення типів змішаних змінних.
let a = 8;
let b = +('2'); тут string примусово перетворена в number
console.log(a + b);
*/

"use strict";
//1
let name, admin;

name = 'Valeria';
admin = name;

console.log(admin);
//2
const days = 7;
const hours = 24;
const minutes = 60;
const seconds = 60;

console.log(days * hours * minutes * seconds);
//3
const message = prompt('Enter your name.');

console.log(message);

