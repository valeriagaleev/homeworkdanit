/*
Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
Функція setTimeout() виконується один раз, а setInterval() виконується безкінечно через заданий проміжок часу і зупинити її виконання можна тільки за допомогою функції clearInterval().
Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
Функція виконається якомога швидше, але після виконання всьго java script.
Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
Тому, що він буде виконуватись безкінечно. А також не відключений таймер дає велике навантаження на браузер.
*/

"use strict";

let imagesWrapper = document.querySelector(".images-wrapper");
let images = document.querySelectorAll(".image-to-show");
let imgInterval;
let x = 0;
const btnStop = document.createElement("button");
btnStop.innerHTML = "Припинити";
imagesWrapper.append(btnStop);
const btnResume = document.createElement("button");
btnResume.innerHTML = "Відновити показ";
imagesWrapper.append(btnResume);

function showImages() {
    imgInterval = setInterval(() => {
    images.forEach((img) => (img.style.display = "none"));
    const item = images[x];
    item.style.display = "";
    console.log(item);
    x++;
    if (x > images.length-1) {
      x = 0;
    }
  }, 3000);
}

btnStop.addEventListener("click", function () {
  if (imgInterval) {
    clearInterval(imgInterval);
  }
  console.log(imgInterval);
});

btnResume.addEventListener("click", function () {
  showImages();
});
showImages();

