const user1 = {
    name: 'Valeria',
    lastName: 'Yevenko',
    hobby: null,
    pets: {
        dog: 'Teddy',
        cat: 'Luna'
    }
}

function clone (obj) {
    let obj2 = {}
    for (let key in obj) {
        if (typeof obj[key] === 'object' && obj[key] !== null) {
            obj2[key] = clone(obj[key]);
        } else {
            obj2[key] = obj[key]
        }
    }
    return obj2;
}

let copy = clone(user1);
console.log(copy);
copy.pets.dog = 'Max';
copy.lastName = 'Galeeva';
console.log(user1);