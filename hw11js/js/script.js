"use strict";

window.addEventListener('load', () => {
    let pass = document.querySelectorAll('input');
    let icons = document.querySelectorAll('.fas');

    icons.forEach(el => el.addEventListener('click', function () {
        if (el.classList.contains('fa-eye')) {
            el.classList.remove('fa-eye');
            el.classList.add('fa-eye-slash');
            el.previousElementSibling.type = 'password';
        } else {
            el.classList.remove('fa-eye-slash');
            el.classList.add('fa-eye');
            el.previousElementSibling.type = 'text';
        }
    }))

    document.querySelector('form').addEventListener('submit', function (event) {
        event.preventDefault();
        document.querySelector('#msg') ? document.querySelector('#msg').remove() : false;
        let message = function (str) {
            document.querySelector('button').insertAdjacentHTML('beforebegin', `<div id="msg">${str}</div>`)
        };
        return (!pass[0].value || pass[0].value !== pass[1].value) ? message('Password is empty or not equal!') : message('You are welcome!'); this.submit();
    })
})
